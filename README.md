Sonic-Pi 3 now allows you to send and receive OSC messages to and from other computers.
You simply need to enable the "Receive remote OSC messages" box under Preferences/IO.

An issue still remains though. Namely the ability to sync multiple computers running Sonic-Pi to a metronome. 
This is important if you want to develop with friends laptop orchestra performances which are beat oriented.

Ideally, each time a player hit "Run" on their laptop, all loops should sync to a master beat shared across all the performers.

Hereafter I suggest a simple workaround to achieve such synchronicity.

NOTE: The example below works for two performers. Unless Sonic-pi supports multicasting and I am not aware, in order to sync multiple computers (3 or more) possible solutions are: 

1) Computer/Performer 1 sending OSCs to a router which then re-route the OSC messages to all machines.   
2) Have Computer/Performer 2 rerouting incoming OSC to Computer/Preformer 3 etc. However, we may incur in synchronicity issues due to network time-lag. 