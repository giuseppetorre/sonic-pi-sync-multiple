# COMPUTER/PERFORMER 1 - BUFFER 0

use_osc "10.100.11.230", 4559
sleep 1 # give sonic-pi a little time to setup stuff...just in case

in_thread do
  loop do
    cue :tick #This cue is used to sync other buffers. In other word PERFORMER 1 sends the beat but can also perform in 
              # other buffers (see file below osc-beat-03
    
    # The following sequnce of 1s and 0s simulates a 4/4 time signature at 60bpm 
    osc "/trigger", 1
    sleep 1
    osc "/trigger", 0
    sleep 1
    osc "/trigger", 0
    sleep 1
    osc "/trigger", 0
    sleep 1
    
  end
end