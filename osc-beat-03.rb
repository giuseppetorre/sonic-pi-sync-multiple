# simple drum loop lasting two quarter and resting for further two (so to check synchronicity) 
with_fx :gverb do
  live_loop :cdddr do
    
    sync :tick   # cue in Buffer 0
    play 66, amp: 0.5
    sample :drum_bass_hard
    sleep 0.5
    sample :sn_dub
    sleep 0.5
    sample :drum_bass_hard
    sleep 0.25
    sample :drum_bass_hard
    sleep 0.25
    sample :drum_bass_hard
    sleep 0.25
    sample :sn_dub
    sleep 0.25
    
  end
end