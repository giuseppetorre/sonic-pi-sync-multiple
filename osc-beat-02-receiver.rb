# COMPUTER/PERFORMER 2 - RECEIVES OSC AND SYNC TO COMPUTER 1


live_loop :ciao do
  use_real_time
  a,b = sync "/osc/trigger" 
  
  if a == 1
    use_synth :fm
    play 74 , release: 2
    sleep 1
    play 64 , release: 2
    sleep 1
    play 62 , release: 2
    sleep 1
    play 69 , release: 2
    sleep 0.9  # just a tiny bit less than one to allow the loop and the IF statement to read next incoming 1
    
  else
    sleep 0.1 #just in case there is no 1 we sleep a tiny bit to avoid errors.
  end
end
